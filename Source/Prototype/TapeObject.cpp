// Fill out your copyright notice in the Description page of Project Settings.


#include "TapeObject.h"

// Sets default values
ATapeObject::ATapeObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	//SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	//VisibleComponent->SetupAttachment(RootComponent);
	RootComponent = VisibleComponent;

	static ConstructorHelpers::FObjectFinder<USoundCue>SoundCueAsset(TEXT("SoundCue'/Game/Room2/Audio/Pick_Cue.Pick_Cue'"));
	if (SoundCueAsset.Succeeded())
	{
		Sound = SoundCueAsset.Object;
	}
}

// Called when the game starts or when spawned
void ATapeObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ATapeObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ATapeObject::PickUp_Implementation()
{
	APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		MyCharacter->AddTapes();
		IsPickedUp = true;

		if (Sound != nullptr)
		{
			UGameplayStatics::PlaySound2D(this, Sound);
		}
	}
	//Destroy();
}
