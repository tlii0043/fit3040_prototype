// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CanBeInspected.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Calendar.generated.h"

UCLASS()
class PROTOTYPE_API ACalendar : public AActor, public ICanBeInspected
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACalendar();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Calendar")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};
