// Fill out your copyright notice in the Description page of Project Settings.


#include "KeyDoor.h"

// Sets default values
AKeyDoor::AKeyDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

}

// Called when the game starts or when spawned
void AKeyDoor::BeginPlay()
{
	Super::BeginPlay();

	newRot = GetActorRotation() + FRotator(0.f, Degree, 0.f);
	
}

// Called every frame
void AKeyDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		if (MyCharacter->GetKeys() >= 1)
		{
			CanBeUsed = true;
			if (IsFirst == true)
			{
				FRotator final = FMath::RInterpTo(GetActorRotation(), newRot, DeltaTime, 1.0f);
				SetActorRotation(final.Quaternion());
				//MyCharacter->LoseKeys();
			}
		}
	}
	/*if (CanBeUsed == true)
	{

		FRotator final = FMath::RInterpTo(GetActorRotation(), newRot, DeltaTime, 1.0f);//interp
		SetActorRotation(final.Quaternion());
	}*/
}

void AKeyDoor::Use_Implementation()
{
	/*APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		if (MyCharacter->GetKeys() >= 1) 
		{
			GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Green, TEXT("You are trying to open the door.\nThe Door is opened."));
			CanBeUsed = true;
		}
		else 
		{
			GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Red, TEXT("You are trying to open the door.\nThe Door cannot be opened without the key."));
		}
	}*/

}

