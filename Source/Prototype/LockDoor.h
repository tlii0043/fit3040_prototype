// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
#include "CanBeUsed.h"
#include "CanBeInspected.h"
#include "LockObject.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LockDoor.generated.h"

UCLASS()
class PROTOTYPE_API ALockDoor : public AActor, public ICanBeUsed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALockDoor();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		UStaticMeshComponent* VisibleComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		AActor* Lock;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Door")
		float Degree = 0.0f;
	FRotator newRot;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;

	UPROPERTY(BlueprintReadWrite)
		bool CanBeUsed = false;
	UPROPERTY(BlueprintReadWrite)
		bool IsFirst = false;

};
