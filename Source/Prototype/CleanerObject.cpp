// Fill out your copyright notice in the Description page of Project Settings.


#include "CleanerObject.h"

// Sets default values
ACleanerObject::ACleanerObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(Scene);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<USoundCue>SoundCueAsset(TEXT("SoundCue'/Game/Room2/Audio/Pick_Cue.Pick_Cue'"));
	if (SoundCueAsset.Succeeded())
	{
		Sound = SoundCueAsset.Object;
	}
}

// Called when the game starts or when spawned
void ACleanerObject::BeginPlay()
{
	Super::BeginPlay();
	
	if (!Sound)
	{
		UE_LOG(LogTemp, Error, TEXT("%s don't have sound set."), *GetOwner()->GetName());
	}
}

// Called every frame
void ACleanerObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void ACleanerObject::PickUp_Implementation()
{
	APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		MyCharacter->AddCleaner();
		IsPickedUp = true;

		if (Sound != nullptr)
		{
			UGameplayStatics::PlaySound2D(this, Sound);
		}
	}
	//Destroy();
}
