﻿// Copyright Epic Games, Inc. All Rights Reserved.

#include "PrototypeCharacter.h"
#include "PrototypeProjectile.h"
#include "Animation/AnimInstance.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/InputSettings.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Kismet/GameplayStatics.h"
#include "MotionControllerComponent.h"
#include "XRMotionControllerBase.h" // for FXRMotionControllerBase::RightHandSourceId

DEFINE_LOG_CATEGORY_STATIC(LogFPChar, Warning, All);

//////////////////////////////////////////////////////////////////////////
// APrototypeCharacter

APrototypeCharacter::APrototypeCharacter()
{
	// Set size for collision capsule
	GetCapsuleComponent()->InitCapsuleSize(55.f, 96.0f);

	// set our turn rates for input
	BaseTurnRate = 45.f;
	BaseLookUpRate = 45.f;

	// Create a CameraComponent	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetupAttachment(GetCapsuleComponent());
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;

	// Create a mesh component that will be used when being viewed from a '1st person' view (when controlling this pawn)
	Mesh1P = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("CharacterMesh1P"));
	Mesh1P->SetOnlyOwnerSee(true);
	Mesh1P->SetupAttachment(FirstPersonCameraComponent);
	Mesh1P->bCastDynamicShadow = false;
	Mesh1P->CastShadow = false;
	Mesh1P->SetRelativeRotation(FRotator(1.9f, -19.19f, 5.2f));
	Mesh1P->SetRelativeLocation(FVector(-0.5f, -4.4f, -155.7f));

	// Create a gun mesh component
	FP_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("FP_Gun"));
	FP_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	FP_Gun->bCastDynamicShadow = false;
	FP_Gun->CastShadow = false;
	// FP_Gun->SetupAttachment(Mesh1P, TEXT("GripPoint"));
	FP_Gun->SetupAttachment(RootComponent);

	FP_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	FP_MuzzleLocation->SetupAttachment(FP_Gun);
	FP_MuzzleLocation->SetRelativeLocation(FVector(0.2f, 48.4f, -10.6f));

	// Default offset from the character location for projectiles to spawn
	GunOffset = FVector(100.0f, 0.0f, 10.0f);

	// Note: The ProjectileClass and the skeletal mesh/anim blueprints for Mesh1P, FP_Gun, and VR_Gun 
	// are set in the derived blueprint asset named MyCharacter to avoid direct content references in C++.

	// Create VR Controllers.
	R_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("R_MotionController"));
	R_MotionController->MotionSource = FXRMotionControllerBase::RightHandSourceId;
	R_MotionController->SetupAttachment(RootComponent);
	L_MotionController = CreateDefaultSubobject<UMotionControllerComponent>(TEXT("L_MotionController"));
	L_MotionController->SetupAttachment(RootComponent);

	// Create a gun and attach it to the right-hand VR controller.
	// Create a gun mesh component
	VR_Gun = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("VR_Gun"));
	VR_Gun->SetOnlyOwnerSee(false);			// otherwise won't be visible in the multiplayer
	VR_Gun->bCastDynamicShadow = false;
	VR_Gun->CastShadow = false;
	VR_Gun->SetupAttachment(R_MotionController);
	VR_Gun->SetRelativeRotation(FRotator(0.0f, -90.0f, 0.0f));

	VR_MuzzleLocation = CreateDefaultSubobject<USceneComponent>(TEXT("VR_MuzzleLocation"));
	VR_MuzzleLocation->SetupAttachment(VR_Gun);
	VR_MuzzleLocation->SetRelativeLocation(FVector(0.000004, 53.999992, 10.000000));
	VR_MuzzleLocation->SetRelativeRotation(FRotator(0.0f, 90.0f, 0.0f));		// Counteract the rotation of the VR gun model.

	// Uncomment the following line to turn motion controllers on by default:
	//bUsingMotionControllers = true;

	static ConstructorHelpers::FObjectFinder<USoundCue>OnFlashLightSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/OnFlashLightSound_Cue.OnFlashLightSound_Cue'"));
	if (OnFlashLightSoundCueAsset.Succeeded())
	{
		OnFlashLightSound = OnFlashLightSoundCueAsset.Object;
	}

	static ConstructorHelpers::FObjectFinder<USoundCue>OffFlashLightSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/OffFlashLightSound_Cue.OffFlashLightSound_Cue'"));
	if (OffFlashLightSoundCueAsset.Succeeded())
	{
		OffFlashLightSound = OffFlashLightSoundCueAsset.Object;
	}
}

void APrototypeCharacter::BeginPlay()
{
	// Call the base class  
	Super::BeginPlay();

	//Attach gun mesh component to Skeleton, doing it here because the skeleton is not yet created in the constructor
	FP_Gun->AttachToComponent(Mesh1P, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), TEXT("GripPoint"));

	// Show or hide the two versions of the gun based on whether or not we're using motion controllers.
	if (bUsingMotionControllers)
	{
		VR_Gun->SetHiddenInGame(false, true);
		Mesh1P->SetHiddenInGame(true, true);
	}
	else
	{
		VR_Gun->SetHiddenInGame(true, true);
		Mesh1P->SetHiddenInGame(false, true);
	}
}

//////////////////////////////////////////////////////////////////////////
// Input

void APrototypeCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	// set up gameplay key bindings
	check(PlayerInputComponent);

	// Bind jump events
	//PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	//PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);
	PlayerInputComponent->BindAction("Flashlight", IE_Pressed, this, &APrototypeCharacter::CallFlashlight);

	// Bind fire event
	//PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &APrototypeCharacter::OnFire);

	// Enable touchscreen input
	EnableTouchscreenMovement(PlayerInputComponent);

	PlayerInputComponent->BindAction("ResetVR", IE_Pressed, this, &APrototypeCharacter::OnResetVR);

	// Bind movement events
	PlayerInputComponent->BindAxis("MoveForward", this, &APrototypeCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &APrototypeCharacter::MoveRight);

	// We have 2 versions of the rotation bindings to handle different kinds of devices differently
	// "turn" handles devices that provide an absolute delta, such as a mouse.
	// "turnrate" is for devices that we choose to treat as a rate of change, such as an analog joystick
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("TurnRate", this, &APrototypeCharacter::TurnAtRate);
	PlayerInputComponent->BindAxis("LookUp", this, &APawn::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("LookUpRate", this, &APrototypeCharacter::LookUpAtRate);

	PlayerInputComponent->BindAction("UseOrPick", IE_Released, this, &APrototypeCharacter::PressF);
	PlayerInputComponent->BindAction("Inspect", IE_Released, this, &APrototypeCharacter::PressE);
}

/*void APrototypeCharacter::PressO() 
{
	CallMyTraceOpen();
}*/
void APrototypeCharacter::PressF()
{
	CallMyTraceUseOrPick();
}
void APrototypeCharacter::PressE()
{
	CallMyTraceInspect();
}

void APrototypeCharacter::OnResetVR()
{
	UHeadMountedDisplayFunctionLibrary::ResetOrientationAndPosition();
}

void APrototypeCharacter::BeginTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == true)
	{
		return;
	}
	if ((FingerIndex == TouchItem.FingerIndex) && (TouchItem.bMoved == false))
	{
		//OnFire();
	}
	TouchItem.bIsPressed = true;
	TouchItem.FingerIndex = FingerIndex;
	TouchItem.Location = Location;
	TouchItem.bMoved = false;
}

void APrototypeCharacter::EndTouch(const ETouchIndex::Type FingerIndex, const FVector Location)
{
	if (TouchItem.bIsPressed == false)
	{
		return;
	}
	TouchItem.bIsPressed = false;
}

void APrototypeCharacter::MoveForward(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorForwardVector(), Value);
	}
}

void APrototypeCharacter::MoveRight(float Value)
{
	if (Value != 0.0f)
	{
		// add movement in that direction
		AddMovementInput(GetActorRightVector(), Value);
	}
}

void APrototypeCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void APrototypeCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

bool APrototypeCharacter::EnableTouchscreenMovement(class UInputComponent* PlayerInputComponent)
{
	if (FPlatformMisc::SupportsTouchInput() || GetDefault<UInputSettings>()->bUseMouseForTouch)
	{
		PlayerInputComponent->BindTouch(EInputEvent::IE_Pressed, this, &APrototypeCharacter::BeginTouch);
		PlayerInputComponent->BindTouch(EInputEvent::IE_Released, this, &APrototypeCharacter::EndTouch);

		//Commenting this out to be more consistent with FPS BP template.
		//PlayerInputComponent->BindTouch(EInputEvent::IE_Repeat, this, &APrototypeCharacter::TouchUpdate);
		return true;
	}
	
	return false;
}


//***************************************************************************************************
//** Trace functions - used to detect items we are looking at in the world
//***************************************************************************************************
//***************************************************************************************************

//***************************************************************************************************
//** Trace() - called by our CallMyTrace() function which sets up our parameters and passes them through
//***************************************************************************************************

bool APrototypeCharacter::Trace(UWorld* World,TArray<AActor*>& ActorsToIgnore,const FVector& Start,const FVector& End,FHitResult& HitOut,ECollisionChannel CollisionChannel = ECC_Pawn,bool ReturnPhysMat = false) 
{
	if (!World)
	{
		return false;
	}

	FCollisionQueryParams TraceParams(FName(TEXT("My Trace")), true, ActorsToIgnore[0]);

	TraceParams.bTraceComplex = true;
	TraceParams.bReturnPhysicalMaterial = ReturnPhysMat;
	TraceParams.AddIgnoredActors(ActorsToIgnore);

	//const FName TraceTag("MyTraceTag");
	//World->DebugDrawTraceTag = TraceTag;
	//TraceParams.TraceTag = TraceTag;

	HitOut = FHitResult(ForceInit);

	World->LineTraceSingleByChannel
	(
		HitOut,		//result
		Start,   	//start
		End,        //end
		CollisionChannel, //collision channel
		TraceParams
	);

	return (HitOut.GetActor() != NULL);
}

//***************************************************************************************************
//** CallMyTrace() - sets up our parameters and then calls our Trace() function
//***************************************************************************************************

void APrototypeCharacter::CallMyTraceUseOrPick()
{
	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	const FVector End = Start + ForwardVector * 700;


	FHitResult HitData(ForceInit);

	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(this);

	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		if (HitData.GetActor())
		{
			//ProcessTraceHit(HitData);
			if (HitData.GetActor()->GetClass()->ImplementsInterface(UCanBeUsed::StaticClass()))
			{
				ICanBeUsed::Execute_Use(HitData.GetActor());
			}
			else if (HitData.GetActor()->GetClass()->ImplementsInterface(UCanBePickedUp::StaticClass()))
			{
				ICanBePickedUp::Execute_PickUp(HitData.GetActor());
			}
		}
		else
		{
			// The trace did not return an Actor, An error has occurred
			UE_LOG(LogClass, Warning, TEXT("Error"));
		}
		//testing statement
		//UE_LOG(LogClass, Warning, TEXT("F - testing statement. %s"), *HitData.GetActor()->GetClass()->GetFName().ToString());
	}
	else
	{
		// We did not hit an Actor
	}
}

void APrototypeCharacter::CallMyTraceInspect()
{
	const FVector Start = FirstPersonCameraComponent->GetComponentLocation();
	const FVector ForwardVector = FirstPersonCameraComponent->GetForwardVector();
	const FVector End = Start + ForwardVector * 700;


	FHitResult HitData(ForceInit);

	TArray<AActor*> ActorsToIgnore;
	ActorsToIgnore.Add(this);

	if (Trace(GetWorld(), ActorsToIgnore, Start, End, HitData, ECC_Visibility, false))
	{
		//if (HitData.GetActor() && (FVector::Dist(GetActorLocation(), HitData.GetActor()->GetActorLocation()) <= 240.0f))
		if (HitData.GetActor())
		{
			//ProcessTraceHit(HitData);
			if (HitData.GetActor()->GetClass()->ImplementsInterface(UCanBeInspected::StaticClass()))
			{
				ICanBeInspected::Execute_Inspect(HitData.GetActor());
			}
		}
		else
		{
			// The trace did not return an Actor, An error has occurred
			UE_LOG(LogClass, Warning, TEXT("Error"));
		}
		//testing statement
		//UE_LOG(LogClass, Warning, TEXT("E - testing statement. %s"), *HitData.GetActor()->GetClass()->GetFName().ToString());
	}
	else
	{
		// We did not hit an Actor
	}
}

void APrototypeCharacter::CallFlashlight()
{
	if (GetFlashlight() > 0) 
	{
		if (IsLight == false) 
		{
			IsLight = true;
			if (OnFlashLightSound != nullptr)
			{
				UGameplayStatics::PlaySound2D(this, OnFlashLightSound);
			}
		}
		else 
		{
			IsLight = false;
			if (OffFlashLightSound != nullptr)
			{
				UGameplayStatics::PlaySound2D(this, OffFlashLightSound);
			}
		}
	}
	else 
	{
		GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Red, TEXT("You need the Flashlight."));
	}
}

void APrototypeCharacter::AddTapes() 
{
	Tapes++;
}

int APrototypeCharacter::GetTapes()
{
	return Tapes;
}

void APrototypeCharacter::AddCleaner()
{
	Cleaner++;
}

int APrototypeCharacter::GetCleaner()
{
	return Cleaner;
}

void APrototypeCharacter::AddKeys()
{
	Keys++;
}

void APrototypeCharacter::LoseKeys()
{
	Keys--;
}

int APrototypeCharacter::GetKeys()
{
	return Keys;
}

void APrototypeCharacter::AddFlashlight()
{
	Flashlight ++;
}

int APrototypeCharacter::GetFlashlight()
{
	return Flashlight;
}