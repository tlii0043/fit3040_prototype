// Fill out your copyright notice in the Description page of Project Settings.


#include "LockObject.h"

// Sets default values
ALockObject::ALockObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	//SetRootComponent(Scene);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	//VisibleComponent->SetupAttachment(RootComponent);
	RootComponent = VisibleComponent;

	//create related widgets
	LockUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("LockUICompo"));
}

// Called when the game starts or when spawned
void ALockObject::BeginPlay()
{
	Super::BeginPlay();
	
	if (Lock != nullptr)
	{
		// if 0, create a new DigitLockUI for this LockObjectbject
		if (IsDigit)
		{
			UDigitLock* lock = CreateWidget<UDigitLock>(GetWorld()->GetGameInstance(), Lock);
			if (lock != nullptr) 
			{
				if (LockUICompo != nullptr) 
				{
					LockUICompo->SetWidget(lock);
				}
			}
		}
		// if 1, create a new NumLockUI for this LockObjectbject
		else if (IsLetter)
		{
			ULetterLock* lock = CreateWidget<ULetterLock>(GetWorld()->GetGameInstance(), Lock);
			if (lock != nullptr)
			{
				if (LockUICompo != nullptr)
				{
					LockUICompo->SetWidget(lock);
				}
			}
		}
	}

}

// Called every frame
void ALockObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (IsDigit)
	{
		if (LockUICompo != nullptr) 
		{
			UDigitLock* lock = Cast<UDigitLock>(LockUICompo->GetUserWidgetObject());
			if (lock != nullptr)
			{
				IsUnlocked = lock->IsUnlocked;
			}
		}
	}
	else if (IsLetter)
	{
		if (LockUICompo != nullptr)
		{
			ULetterLock* lock = Cast<ULetterLock>(LockUICompo->GetUserWidgetObject());
			if (lock != nullptr)
			{
				IsUnlocked = lock->IsUnlocked;
			}
		}
	}
}

/*void ALockObject::Use_Implementation() 
{
	CanBeUsed = true;
}*/

