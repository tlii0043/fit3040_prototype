// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Components/StaticMeshComponent.h"
#include "CanBeUsed.h"
#include "CanBeInspected.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PhoneObject.generated.h"

UCLASS()
class PROTOTYPE_API APhoneObject : public AActor, public ICanBeUsed, public ICanBeInspected
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APhoneObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Phone")
		UStaticMeshComponent* VisibleComponent;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
		class UWidgetComponent* PhoneUICompo;
	TSubclassOf<UUserWidget> UI;

	UPROPERTY(BlueprintReadWrite)
		bool CanBeSeen = false;
};
