// Fill out your copyright notice in the Description page of Project Settings.


#include "Cabinet.h"
#include "Sound/SoundCue.h"

// Sets default values
ACabinet::ACabinet()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	RootComponent = VisibleComponent;

	static ConstructorHelpers::FObjectFinder<USoundCue>OpenSoundCueAsset(TEXT("SoundCue'/Game/Room1/Audio/OpenCloseCabinet_Sound.OpenCloseCabinet_Sound'"));
	static ConstructorHelpers::FObjectFinder<USoundCue>LockedSoundCueAsset(TEXT("SoundCue'/Game/Room1/Audio/LockedDoor_Sound.LockedDoor_Sound'"));
	if (OpenSoundCueAsset.Succeeded())
	{
		OpenSound = OpenSoundCueAsset.Object;
	}
	if (LockedSoundCueAsset.Succeeded())
	{
		LockedSound = LockedSoundCueAsset.Object;
	}
}

// Called when the game starts or when spawned
void ACabinet::BeginPlay()
{
	Super::BeginPlay();
	
	//get the start position of the door
	StartingPosition = GetActorLocation();
	EndPosition = StartingPosition + (GetActorForwardVector() * Direction * Distance);
}

// Called every frame
void ACabinet::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (CanBeOpened) 
	{
		//check if the door is opened
		if (!IsOpened) 
		{
			if (FVector::Dist(GetActorLocation(), EndPosition) >= 3.0f)
			{
				const FVector DirectionVector = GetActorForwardVector() * Direction;
				FVector CurrentPosition = GetActorLocation();
				FVector TargetPosition = StartingPosition + (DirectionVector * Distance);
				CurrentPosition += DirectionVector * Speed * DeltaTime;
				SetActorLocation(CurrentPosition);
			}
			else
			{
				IsOpened = true;
				CanBeOpened = false;
			}
		}
		else 
		{
			if (FVector::Dist(GetActorLocation(), StartingPosition) >= 3.0f)
			{
				const FVector DirectionVector = GetActorForwardVector() * Direction * -1;
				FVector CurrentPosition = GetActorLocation();
				FVector TargetPosition = EndPosition - (DirectionVector * Distance);
				CurrentPosition += DirectionVector * Speed * DeltaTime;
				SetActorLocation(CurrentPosition);
			}
			else
			{
				IsOpened = false;
				CanBeOpened = false;
			}
		}

	}
}

void ACabinet::Use_Implementation()
{
	if (!IsLocked) 
	{
		CanBeOpened = true;
		if (OpenSound != nullptr)
		{
			UGameplayStatics::PlaySound2D(this, OpenSound);
		}
	}
	else 
	{
		APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
		if (MyCharacter)
		{
			if (MyCharacter->GetKeys() >= 1)
			{
				IsLocked = false;
				CanBeOpened = true;
				if (OpenSound != nullptr)
				{
					UGameplayStatics::PlaySound2D(this, OpenSound);
				}
				MyCharacter->LoseKeys();
			}
			else 
			{
				LockUI = true;
				if (LockedSound != nullptr)
				{
					UGameplayStatics::PlaySound2D(this, LockedSound);
				}
			}
		}
	}
}

