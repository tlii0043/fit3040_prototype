// Fill out your copyright notice in the Description page of Project Settings.


#include "SocketObject.h"

// Sets default values
ASocketObject::ASocketObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	// Box Component
	/*BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	BoxComponent->SetupAttachment(RootComponent);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &ASocketObject::OnOverlapBegins);*/
}

// Called when the game starts or when spawned
void ASocketObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASocketObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/*void ASocketObject::OnOverlapBegins(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr)
	{
		ABoidActor* boid = Cast<ABoidActor>(OtherActor);
		if (boid)
		{
			if (boid == targetBoid)
			{
				ChaseTimer = 0.0f;
				boid->IsEaten = true;
				GEngine->AddOnScreenDebugMessage(-1, 8.f, FColor::Red, FString::Printf(TEXT("A boid is eaten by predator")));
				targetBoid = nullptr;
				IsWait = true;
				GetWorldTimerManager().SetTimer(Timer, this, &APredator::Pause, 5.0f, true, 5.0f);
			}
		}
	}
}*/
