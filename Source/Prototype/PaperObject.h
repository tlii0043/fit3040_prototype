// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PrototypeCharacter.h"
#include "Components/StaticMeshComponent.h"
//#include "CanBeUsed.h"
#include "CanBeInspected.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PaperObject.generated.h"

UCLASS()
class PROTOTYPE_API APaperObject : public AActor, public ICanBeInspected
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APaperObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Paper")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;*/

	/*UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Inspect")
		void Inspect();
	virtual void Inspect_Implementation() override;*/

	// Paper UI
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
		class UWidgetComponent* PaperUICompo;
	TSubclassOf<UUserWidget> UI;

	UPROPERTY(BlueprintReadWrite)
		bool CanBeSeen = false;*/

};
