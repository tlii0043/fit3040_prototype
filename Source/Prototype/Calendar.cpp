// Fill out your copyright notice in the Description page of Project Settings.


#include "Calendar.h"

// Sets default values
ACalendar::ACalendar()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	RootComponent = VisibleComponent;
}

// Called when the game starts or when spawned
void ACalendar::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACalendar::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

