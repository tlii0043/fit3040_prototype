// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "PrototypeCharacter.h"
#include "CanBeUsed.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Cabinet.generated.h"

UCLASS()
class PROTOTYPE_API ACabinet : public AActor, public ICanBeUsed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACabinet();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Cabinet")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cabinet, meta = (AllowPrivateAccess = "true"))
		float Speed = 10.0f;                     //the speed of door movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cabinet, meta = (AllowPrivateAccess = "true"))
		float Distance = 30.0f;                  //the distance of door movement
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Cabinet, meta = (AllowPrivateAccess = "true"))
		bool IsLocked = false;


	float Direction = 1.0f;

	FVector StartingPosition;     //where the door start to move
	FVector EndPosition;         //where the door start to move

	UPROPERTY(BlueprintReadWrite)
		bool LockUI = false;

	bool CanBeOpened = false;
	bool IsOpened = false;

	class USoundCue* OpenSound;
	class USoundCue* LockedSound;
};
