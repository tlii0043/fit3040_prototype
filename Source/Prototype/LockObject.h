// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "DigitLock.h"
#include "LetterLock.h"
#include "Components/StaticMeshComponent.h"
#include "CanBeUsed.h"
#include "CanBeInspected.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LockObject.generated.h"

UCLASS()
class PROTOTYPE_API ALockObject : public AActor, public ICanBeUsed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ALockObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lock")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lock")
		bool IsUnlocked = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
		class UWidgetComponent* LockUICompo;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lock")
		bool IsDigit = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lock")
		bool IsLetter = false;
	TSubclassOf<UUserWidget> Lock;
};
