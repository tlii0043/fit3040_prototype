// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/GameplayStatics.h"
#include "PrototypeCharacter.h"
#include "SocketObject.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "CanBeInspected.h"
#include "CanBeUsed.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProjectorObject.generated.h"

UCLASS()
class PROTOTYPE_API AProjectorObject : public AActor, public ICanBeUsed
{
	GENERATED_BODY()

public:	
	// Sets default values for this actor's properties
	AProjectorObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projector")
		UStaticMeshComponent* VisibleComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Projector")
		UStaticMeshComponent* Plug;
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		UBoxComponent* BoxComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;


	UPROPERTY(BlueprintReadWrite)
		bool IsUse = false;

	UPROPERTY(BlueprintReadWrite)
		bool IsCharged = false;
	UPROPERTY(BlueprintReadWrite)
		bool IsActived = false;

	UFUNCTION()
	void OnOverlapBegins(UPrimitiveComponent* OverlapComponent,
		AActor* OtherActor, UPrimitiveComponent* OtherComponent,
		int32 OtherBodyIndex, bool bFromSweep,
		const FHitResult& SweepResult);

	class USoundCue* ChargingSound;
	class USoundCue* ActivatingSound;
	class USoundCue* NoPowerSound;
	class USoundCue* TapeSound;
	class USoundCue* ActiveElevatorSound;
};
