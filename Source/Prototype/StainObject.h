// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "PrototypeCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "CanBeUsed.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "StainObject.generated.h"

UCLASS()
class PROTOTYPE_API AStainObject : public AActor, public ICanBeUsed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AStainObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Lock")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();
	virtual void Use_Implementation() override;

	UPROPERTY(BlueprintReadWrite)
		bool CanBeUsed = false;
	UPROPERTY(BlueprintReadWrite)
		bool IsRemoved = false;

	USoundCue* Sound;
};
