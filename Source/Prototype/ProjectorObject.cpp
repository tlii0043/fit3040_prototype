// Fill out your copyright notice in the Description page of Project Settings.


#include "ProjectorObject.h"
#include "Sound/SoundCue.h"

// Sets default values
AProjectorObject::AProjectorObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	Plug = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Plug Component"));
	Plug->SetupAttachment(RootComponent);
	BoxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Trigger Box"));
	BoxComponent->SetupAttachment(Plug);
	BoxComponent->OnComponentBeginOverlap.AddDynamic(this, &AProjectorObject::OnOverlapBegins);

	VisibleComponent->SetSimulatePhysics(true);
	Plug->SetSimulatePhysics(true);

	//Sound
	//Charging
	static ConstructorHelpers::FObjectFinder<USoundCue>ChargingSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/ChargingSound_Cue.ChargingSound_Cue'"));
	if (ChargingSoundCueAsset.Succeeded())
		ChargingSound = ChargingSoundCueAsset.Object;
	//Active Projector
	static ConstructorHelpers::FObjectFinder<USoundCue>ActivatingSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/MixSound_Cue.MixSound_Cue'"));
	if (ActivatingSoundCueAsset.Succeeded())
		ActivatingSound = ActivatingSoundCueAsset.Object;
	//Out of power
	static ConstructorHelpers::FObjectFinder<USoundCue>NoPowerSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/NoPowerSound_Cue.NoPowerSound_Cue'"));
	if (NoPowerSoundCueAsset.Succeeded())
		NoPowerSound = NoPowerSoundCueAsset.Object;
	//Elevator
	static ConstructorHelpers::FObjectFinder<USoundCue>ActiveElevatorSoundCueAsset(TEXT("SoundCue'/Game/Room4/Audio/ElevatorSound_Cue.ElevatorSound_Cue'"));
	if (ActiveElevatorSoundCueAsset.Succeeded())
		ActiveElevatorSound = ActiveElevatorSoundCueAsset.Object;
}

// Called when the game starts or when spawned
void AProjectorObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AProjectorObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AProjectorObject::Use_Implementation()
{
	IsUse = true;

	if (IsCharged) 
	{
		APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
		if (MyCharacter)
		{
			if (MyCharacter->GetTapes() >= 4)
			{
				IsActived = true;
				// Activated Audio
				if (ActivatingSound != nullptr)
					UGameplayStatics::PlaySound2D(this, ActivatingSound);
				//Actived elevator sound
				//if (ActiveElevatorSound != nullptr)
				//	UGameplayStatics::PlaySound2D(this, ActiveElevatorSound);
			}
		}
	}
	else 
	{
		// Out Of Charge Audio
		if (NoPowerSound != nullptr)
			UGameplayStatics::PlaySound2D(this, NoPowerSound);
	}
}

void AProjectorObject::OnOverlapBegins(UPrimitiveComponent* OverlapComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor != nullptr)
	{
		ASocketObject* socket = Cast<ASocketObject>(OtherActor);
		if (socket)
		{
			Plug->SetWorldLocation(socket->GetActorLocation() + FVector(0,0,10));
			IsCharged = true;
			// Charge Audio
			if (ChargingSound != nullptr)
				UGameplayStatics::PlaySound2D(this, ChargingSound);
			Plug->SetSimulatePhysics(false);                //plug cannot move anymore
			//VisibleComponent->SetSimulatePhysics(false);    //projector cannot move anymore
		}
	}
}
