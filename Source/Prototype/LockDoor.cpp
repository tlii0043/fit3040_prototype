// Fill out your copyright notice in the Description page of Project Settings.


#include "LockDoor.h"
//#include "Sound/SoundCue.h"
//#include <Runtime/Engine/Classes/Kismet/GameplayStatics.h>

// Sets default values
ALockDoor::ALockDoor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);


	
}

// Called when the game starts or when spawned
void ALockDoor::BeginPlay()
{
	Super::BeginPlay();

	newRot = GetActorRotation() + FRotator(0.f, Degree, 0.f);
}

// Called every frame
void ALockDoor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ALockObject* lock = Cast<ALockObject>(Lock);
	if (lock)
	{
		if (lock->IsUnlocked == true)
		{
			CanBeUsed = true;
			if (IsFirst == true) 
			{
				FRotator final = FMath::RInterpTo(GetActorRotation(), newRot, DeltaTime, 1.0f);
				SetActorRotation(final.Quaternion());
			}
		}
	}
	/*if (CanBeUsed == true)
	{
		FRotator final = FMath::RInterpTo(GetActorRotation(), newRot, DeltaTime, 1.0f);//interp
		SetActorRotation(final.Quaternion());
	}*/
}

void ALockDoor::Use_Implementation() 
{
	//GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Red, TEXT("Door impete"));
	//USoundCue* Sound = LoadObject<USoundCue>(this, TEXT("/Game/Room1/Audio/Keyboard_Cue.Keyboard_Cue"));
	//if (Sound)
	//{
	//	UGameplayStatics::PlaySound2D(this,Sound);
	//}

	/*ALockObject* lock = Cast<ALockObject>(Lock);
	if (lock)
	{
		if (lock->IsUnlocked == true)
		{
			GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Green, TEXT("You are trying to open the door.\nThe Door is opened."));
			CanBeUsed = true;
		}
		else
		{
			GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Red, TEXT("You are trying to open the door.\nThe Door cannot be opened without the correct code."));
		}
	}*/

}

