// Fill out your copyright notice in the Description page of Project Settings.


#include "StainObject.h"

// Sets default values
AStainObject::AStainObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	static ConstructorHelpers::FObjectFinder<USoundCue>SoundCueAsset(TEXT("SoundCue'/Game/Room2/Audio/Spray_Cue.Spray_Cue'"));
	if (SoundCueAsset.Succeeded())
	{
		Sound = SoundCueAsset.Object;
	}
}

// Called when the game starts or when spawned
void AStainObject::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AStainObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AStainObject::Use_Implementation()
{
	CanBeUsed = true;
	APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	if (MyCharacter)
	{
		if (MyCharacter->GetCleaner() > 0) 
		{
			IsRemoved = true;
			if (Sound != nullptr)
			{
				UGameplayStatics::PlaySound2D(this, Sound);
			}
			
		}
	}
}


