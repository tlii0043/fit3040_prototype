// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "LetterLock.generated.h"

/**
 * 
 */
UCLASS()
class PROTOTYPE_API ULetterLock : public UUserWidget
{
	GENERATED_BODY()
	
public:

	UPROPERTY(BlueprintReadWrite)
		bool IsUnlocked = false;

};
