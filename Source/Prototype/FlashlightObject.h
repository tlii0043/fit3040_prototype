// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "Components/StaticMeshComponent.h"
#include "PrototypeCharacter.h"
#include "CanBeUsed.h"
#include "CanBePickedUp.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "FlashlightObject.generated.h"

UCLASS()
class PROTOTYPE_API AFlashlightObject : public AActor, public ICanBePickedUp, public ICanBeInspected
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AFlashlightObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Flashlight")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "PickUp")
		void PickUp();
	virtual void PickUp_Implementation() override;

	USoundCue* Sound;
};
