// Fill out your copyright notice in the Description page of Project Settings.


#include "PhoneObject.h"

// Sets default values
APhoneObject::APhoneObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	//Scene->SetupAttachment(RootComponent);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	//create related widgets
	PhoneUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("PhoneUICompo"));

}

// Called when the game starts or when spawned
void APhoneObject::BeginPlay()
{
	Super::BeginPlay();

	if (UI != nullptr)
	{
		UUserWidget* phone = CreateWidget<UUserWidget>(GetWorld()->GetGameInstance(), UI);
		if (UI != nullptr)
		{
			if (PhoneUICompo != nullptr)
			{
				PhoneUICompo->SetWidget(phone);
			}
		}
	}
}

// Called every frame
void APhoneObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void APhoneObject::Use_Implementation()
{
	GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Green, TEXT("You've opened the Phone"));
	//APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	CanBeSeen = true;

}


