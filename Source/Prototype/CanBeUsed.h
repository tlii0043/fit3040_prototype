// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "CanBeUsed.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UCanBeUsed : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class PROTOTYPE_API ICanBeUsed
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Use")
		void Use();

};
