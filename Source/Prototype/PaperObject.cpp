// Fill out your copyright notice in the Description page of Project Settings.


#include "PaperObject.h"

// Sets default values
APaperObject::APaperObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	//SetRootComponent(Scene);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	//VisibleComponent->SetupAttachment(RootComponent);
	RootComponent = VisibleComponent;

	//create related widgets
	//PaperUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("PaperUICompo"));
	//InteractionUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("InteractionUICompo"));

}

// Called when the game starts or when spawned
void APaperObject::BeginPlay()
{
	Super::BeginPlay();

	/*if (UI != nullptr)
	{
		UUserWidget* paper = CreateWidget<UUserWidget>(GetWorld()->GetGameInstance(), UI);
		if (UI != nullptr)
		{
			if (PaperUICompo != nullptr)
			{
				PaperUICompo->SetWidget(paper);
			}
		}
	}*/
}

// Called every frame
void APaperObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

/*void APaperObject::Use_Implementation()
{
	GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Green, TEXT("You've picked up the Paper"));
	APrototypeCharacter* MyCharacter = Cast<APrototypeCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));
	
	CanBeSeen = true;

	//Destroy();
}*/

/*void APaperObject::Inspect_Implementation()
{
	GEngine->AddOnScreenDebugMessage(0, 5.0f, FColor::Green, TEXT("Interace!"));
}*/

