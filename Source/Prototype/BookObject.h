// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Blueprint/UserWidget.h"
#include "Components/WidgetComponent.h"
#include "Kismet/GameplayStatics.h"
#include "PrototypeCharacter.h"
#include "Components/StaticMeshComponent.h"
#include "CanBeUsed.h"
#include "CanBeInspected.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BookObject.generated.h"

UCLASS()
class PROTOTYPE_API ABookObject : public AActor, public ICanBeInspected, public ICanBeUsed
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABookObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Book")
		UStaticMeshComponent* VisibleComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Paper UI
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = UI)
		class UWidgetComponent* BookUICompo;
	TSubclassOf<UUserWidget> UI;

	UPROPERTY(BlueprintReadWrite)
		bool CanBeSeen = false;

};
