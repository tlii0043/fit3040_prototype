// Fill out your copyright notice in the Description page of Project Settings.


#include "BookObject.h"

// Sets default values
ABookObject::ABookObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	USceneComponent* Scene = CreateDefaultSubobject<USceneComponent>(TEXT("Root Component"));
	SetRootComponent(Scene);
	VisibleComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Visible Component"));
	VisibleComponent->SetupAttachment(RootComponent);

	//create related widgets
	BookUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("BookUICompo"));
	//InteractionUICompo = CreateDefaultSubobject<UWidgetComponent>(TEXT("InteractionUICompo"));
}

// Called when the game starts or when spawned
void ABookObject::BeginPlay()
{
	Super::BeginPlay();

	if (UI != nullptr)
	{
		UUserWidget* book = CreateWidget<UUserWidget>(GetWorld()->GetGameInstance(), UI);
		if (book != nullptr)
		{
			if (BookUICompo != nullptr)
			{
				BookUICompo->SetWidget(book);
			}
		}
	}
}

// Called every frame
void ABookObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

