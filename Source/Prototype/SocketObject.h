// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Components/StaticMeshComponent.h"
//#include "Components/BoxComponent.h"
#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SocketObject.generated.h"

UCLASS()
class PROTOTYPE_API ASocketObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASocketObject();

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Socket")
		UStaticMeshComponent* VisibleComponent;

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	//	UBoxComponent* BoxComponent;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*UFUNCTION()
		void OnOverlapBegins(UPrimitiveComponent* OverlapComponent,
			AActor* OtherActor, UPrimitiveComponent* OtherComponent,
			int32 OtherBodyIndex, bool bFromSweep,
			const FHitResult& SweepResult);*/

};
